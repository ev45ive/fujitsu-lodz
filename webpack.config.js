var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin')
var path = require('path');

module.exports = {
    entry:{
        main: './src/main.ts'
    },
    output:{
        path: path.join(__dirname, 'dist'),
        filename:'bundle.js'
        //filename:'[name].[hash].js'
    },
    resolve:{
        extensions: ['.ts','.js']
    },
    module:{
        rules:[
            { test:/\.ts$/, use:'awesome-typescript-loader' },
            { test: /\.css$/, use: ExtractTextWebpackPlugin.extract({
                    fallback:'style-loader',
                    use: 'css-loader'
                })
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                use: {
                    loader: 'file-loader',
                    options:{
                        'name':'assets/[name].[hash].[ext]'
                    }
                }
            }
        ]
    },
    plugins:[
        new ExtractTextWebpackPlugin('all_styles.css'),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
    ]
}