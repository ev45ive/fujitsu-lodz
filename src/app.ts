export enum Options {
    INFO = 123,
    WARNING = 234,
    DEBUG = 456,
}

class Application{
    version = '0.0.2';

    //name;
    constructor(private name){
        //this.name = name;
    }
    option:Options = Options.INFO;
    
    configure(option:Options){
        this.option = option;
    }
    run(){
        console.log('Version: '+this.version)
    }
}
var secret = 'placki!'

//module.exports = Application;
export default Application;